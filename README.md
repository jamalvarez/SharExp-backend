# E-commerce - Reviews & Ratings

![Generic badge](https://img.shields.io/badge/USES-JS-yellow.svg?style=for-the-badge)

> Backend Project for Falabella Reviews & Ratings

## Prerequisites

- NodeJS v10.16.0 and above
- Mongodb v3.6.2 and above

## Installation

To get started with the project, first **clone** the project from gitlab

```
$ git clone git@git.fala.cl:hpagadimarri/hackathon2020-backend.git
```

after cloning

```
$ cd hackathon2020-backend
```

Package installation

```
npm install
```

After the above command runs successfully, the **dependencies** get installed


## To Run locally in docker container

Running MongoDB as a Docker container

```
docker run -d mongo
```

To Run Server

```
npm run dev
```

To Run Build

```
npm run build
```

To Run Server

```
npm run start
```


## Usage
List of API requests


|  METHOD | URL  | DESCRIPTION |
|---|---|---|
|POST | /ratingsAndReviews | Add a new RatingsAndReviews |
|GET | /ratingsAndReviews/ |Get all RatingsAndReviews for a specific entity |
|GET | /averageRatings  | Get average Ratings for a specific entity |
|GET | /review  | Get all RatingsAndReviews for a specific entity |
|GET | /myReviews  | Get all RatingsAndReviews for a specific author |
|GET | /v2/averageRatings/  | Get enhanced average Ratings for a specific entity |
|GET | /v2/reviewsByStatus/  | Get all RatingsAndReviews for in a specific status |
|PUT | /reviewStatus  | Update the status of ratings |
|POST | /voteReview  | Add a like or report abusive |

Import data script
```
npm run import-data
```
## Swagger Doc
http://localhost:3000/api-docs
