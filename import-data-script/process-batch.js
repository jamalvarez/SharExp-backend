const submitReview = require("./submit-review");

const processBatch = (reviews) => {
  return new Promise((resolve) => {
    const processArray = reviews.map((review) => submitReview(review));
    Promise.all(processArray).then(() => {
      resolve("ok");
    });
  });
};

module.exports = processBatch;
