const readDataFromFile = require("./read-data-from-file");
const processBatch = require("./process-batch");

const batchCount = process.env.BATCH_COUNT || 200;

const startProcess = async () => {
  const t0 = Date.now(); //stores current Timestamp in milliseconds since 1 January 1970 00:00:00 UTC
  const results = await readDataFromFile();
  let counter = 0;
  console.log("total records: ", results.length);
  while (results.length > 0) {
    ++counter;
    console.log("Processing batch: Start =====> ", counter);
    const reviews = results.splice(0, batchCount);
    await processBatch(reviews);
    console.log(`Processed ${reviews.length * counter} records`);
    console.log("Processing batch: End =====> ", counter);
  }
  console.log(`It took ${(Date.now() - t0) / 1000} seconds to complete`); //print elapsed time between stored t0 and now
};

startProcess();
