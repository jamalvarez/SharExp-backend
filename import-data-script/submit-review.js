var request = require("request");

const submitReview = (review) => {
  return new Promise((resolve, reject) => {
    request.post(
      `${process.env.HOST}/ratingsAndReviews`,
      {
        json: {
          author: review["author id"],
          entityId: review["Product ID"],
          description: review["Review Text"],
          rating: review["Rating"],
          title: review["Title"]
        }
      },
      (error, res, body) => {
        if (error) {
          console.error("Error:::", error);
          reject(error);
        } else {
          resolve(body);
        }
        //	console.log(`statusCode: ${res.statusCode}`);
        //	console.log(body);
      }
    );
  });
};

module.exports = submitReview;
