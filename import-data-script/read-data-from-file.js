const csv = require("csv-parser");
const fs = require("fs");

const readDataFromFile = () => {
  const results = [];
  return new Promise((resolve) => {
    fs.createReadStream(__dirname + "/reviews_rating_data.csv")
      .pipe(csv())
      .on("data", (data) => results.push(data))
      .on("end", async () => {
        resolve(results);
      });
  });
};

module.exports = readDataFromFile;
