import Validator from "validatorjs";

import validate from "../helpers/validate";

import { HTTP_BAD_REQUEST } from "../constants";

export default async (req, res, next) => {
  let validationRule = null;
  let requestData = null;

  switch (req.path) {
    case "/ratingsAndReviews":
      validationRule = {
        author: "required|string",
        entityId: "required|string",
        rating: "min:0|max:5|required"
      };
      requestData = req.body;
      break;
    case "/averageRatings":
      validationRule = {
        entityId: "required|string"
      };
      requestData = req.query;
      break;
    case "/v2/averageRatings":
      validationRule = {
        entityId: "required|string"
      };
      requestData = req.query;
      break;

    case "/reviewStatus":
      validationRule = {
        reviewStatus: ["required", { in: [0, 1, 2, 3] }],
        reviewIds: "required|array"
      };
      requestData = req.body;
      break;
    case "/voteReview":
      validationRule = {
        voterId: "required|string",
        reviewId: "required|string",
        voteType: ["required", { in: ["isHelpful", "isAbusive"] }]
      };
      requestData = req.body;
      break;
  }
  const { err, status } = await validate(requestData, validationRule);

  if (!status) {
    res.status(HTTP_BAD_REQUEST).send({
      code: HTTP_BAD_REQUEST,
      message: "HTTP_BAD_REQUEST",
      data: err
    });
  } else if (
    req.url === "/ratingsAndReviews" &&
    ![1, 2, 3, 4, 5].includes(Number(requestData.rating))
  ) {
    res.status(HTTP_BAD_REQUEST).send({
      code: HTTP_BAD_REQUEST,
      message: "HTTP_BAD_REQUEST",
      data: "rating should be of 1,2,3,4,5"
    });
  } else {
    next();
  }
};
