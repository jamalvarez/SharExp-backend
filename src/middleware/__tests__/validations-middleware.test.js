import validationMiddleware from "../validations-middleware";

describe("Validatin middleware", () => {
  describe("/averageRatings", () => {
    let res, next;
    beforeEach(() => {
      res = {
        status: jest.fn(() => ({ send: () => {} }))
      };
      next = jest.fn();
    });
    test("it goes to the following middleware by calling next()", async () => {
      const req = {
        url: "/averageRatings",
        query: {
          entityId: "100"
        }
      };
      await validationMiddleware(req, res, next);
      expect(next).toHaveBeenCalled();
    });
  });

  describe("/ratingsAndReviews", () => {
    let res, next;
    beforeEach(() => {
      res = {
        status: jest.fn(() => ({ send: () => {} }))
      };
      next = jest.fn();
    });
    test("it goes to the following middleware by calling next()", async () => {
      const req = {
        url: "/ratingsAndReviews",
        body: {
          entityId: "100",
          author: "someauthor",
          rating: 1
        }
      };
      await validationMiddleware(req, res, next);
      expect(next).toHaveBeenCalled();
    });
  });
});
