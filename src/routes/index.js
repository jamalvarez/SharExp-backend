import validationMiddleware from "../middleware/validations-middleware";
import submitReviews from "../service/submit-reviews";
import getReviewsById from "../service/get-reviews";
import getAverages from "../service/get-averages";
import getAllReviews from "../service/get-all-reviews";
import getMyReviews from "../service/get-my-reviews";
import getReviewsByStatus from "../service/get-reviews-by-status";
import patchReviewStatus from "../service/patch-review-status";
import voteReview from "../service/voting/post-review-vote"

export default (app) => {
  app.get("/health-check", (req, res) => {
    res.json({
      health: "ok"
    });
  });

  app.post("/ratingsAndReviews", validationMiddleware, submitReviews);

  app.get("/ratingsAndReviews/:reviewId", getReviewsById);

  app.get("/averageRatings", validationMiddleware, getAverages);

  app.get("/reviews", getAllReviews);

  app.get("/myReviews", getMyReviews);

  app.get("/v2/averageRatings", validationMiddleware, getAverages);

  app.get("/v2/reviewsByStatus/:reviewStatus", validationMiddleware, getReviewsByStatus)

  app.put("/reviewStatus", validationMiddleware, patchReviewStatus)

  app.post("/voteReview", validationMiddleware, voteReview)
};
