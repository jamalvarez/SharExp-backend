import express from "express";
import cookieParser from "cookie-parser";
import bodyParser from "body-parser";
import swaggerUi from "swagger-ui-express";
import cors from "cors";

const swaggerDocument = require("../docs/swagger.json");

require("dotenv").config();

import { mongoClient } from "./repository/mongo-client";

import registerRoutes from "./routes";

const app = express();
app.use(cors());
app.options("*", (req, res) => {
  res.status(200).send("Preflight request allowed");
});
app.use(cookieParser());
app.use(bodyParser({ limit: "5mb" }));
app.use(bodyParser.json());
app.use(function (req, res, next) {
  console.log(
    `Invoked service: ${req.originalUrl} with arguments: ${JSON.stringify(
      req.method === "GET" ? req.query : req.body
    )} and Headers: ${JSON.stringify(req.headers)}`
  );
  next();
});

app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));

registerRoutes(app);

const port = process.env.PORT;
app.listen(process.env.PORT, () => {
  console.log(`Server listening on port ==> ${port}!`);
});

process.on("exit", beforeExit);

function beforeExit(code) {
  console.log("Process exit event with code: ", code);
  mongoClient.close();
}
