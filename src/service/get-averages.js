import { getRatingByEntity } from "../repository/mongo-client";
import { errorHandler } from "../helpers";
import { HTTP_SUCCESS_CODE } from "../constants";

export default async (req, res) => {
  try {
    const { entityId } = req.query;
    const version = req.path === "/v2/averageRatings" ? "v2" : "v1";
    const response = await processAverageReview(entityId, version);
    res.status(HTTP_SUCCESS_CODE);
    return res.json(response);
  } catch ({ message }) {
    console.error("Error while getting call reviews:", message);
    errorHandler(res, message);
  }
};

export const processAverageReview = async (entityId, version = "v1") => {
  const entityRatings = await getRatingByEntity(entityId);
  let totalNumberOfReviews = 0;
  let averageRating = 0;
  const {
    fiveStarReviewCount = 0,
    oneStarReviewCount = 0,
    fourStarReviewCount = 0,
    threeStarReviewCount = 0,
    twoStarReviewCount = 0
  } = entityRatings;
  totalNumberOfReviews =
    fiveStarReviewCount +
    oneStarReviewCount +
    fourStarReviewCount +
    threeStarReviewCount +
    twoStarReviewCount;
  averageRating =
    (5 * fiveStarReviewCount +
      4 * fourStarReviewCount +
      3 * threeStarReviewCount +
      2 * twoStarReviewCount +
      oneStarReviewCount) /
    totalNumberOfReviews;
  if (version === "v2") {
    return {
      totalNumberOfReviews,
      averageRating: Number(averageRating.toFixed(1)),
      numberOfFiveStarReviews: fiveStarReviewCount,
      numberOfFourStarReviews: fourStarReviewCount,
      numberOfThreeStarReviews: threeStarReviewCount,
      numberOfTwoStarReviews: twoStarReviewCount,
      numberOfOneStarReviews: oneStarReviewCount
    };
  } else {
    return {
      totalNumberOfReviews,
      averageRating: Number(averageRating.toFixed(1))
    };
  }
};
