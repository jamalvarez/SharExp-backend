import { getReviewListBy } from "../repository/mongo-client";
import { HTTP_SUCCESS_CODE } from "../constants";
import { errorHandler } from "../helpers";

const getMyReviews = (author, sortBy, offset, limit) => {
  return new Promise(async (resolve, reject) => {
    try {
      const collection = await getReviewListBy(
        { author },
        { sortBy, offset, limit }
      );
      const sanitizedReviewArray = collection.map((rawReview) => {
        const { _id, reviewId, ...rest } = rawReview;
        return rest;
      });
      resolve(sanitizedReviewArray);
    } catch (err) {
      reject(err);
    }
  });
};

export default async (req, res) => {
  try {
    const { author, sortBy, offset, limit } = req.query;
    const myReviewsResponse = await getMyReviews(author, sortBy, offset, limit);
    res.status(HTTP_SUCCESS_CODE);
    return res.json(myReviewsResponse);
  } catch ({ message }) {
    console.error("Error while getting call reviews:", message);
    errorHandler(res, message);
  }
};
