import { getSingleReviewBy } from "../repository/mongo-client";
import { errorHandler } from "../helpers";
import { HTTP_NOT_FOUND, HTTP_SUCCESS_CODE, DB_COLUMN } from "../constants";

const getReviewById = (reviewId) => {
  return new Promise(async (resolve, reject) => {
    const query = { reviewId };
    try {
      const { _id, ...data } = await getSingleReviewBy(query);
      resolve(data);
    } catch (err) {
      reject(err);
    }
  });
};

export default async (req, res) => {
  try {
    const { reviewId } = req.params;
    const reviewResponse = await getReviewById(reviewId);
    res.status(reviewResponse ? HTTP_SUCCESS_CODE : HTTP_NOT_FOUND);
    return res.json(reviewResponse || {});
  } catch ({ message, code }) {
    console.error("Error while getting call reviews:", message);
    errorHandler(res, message);
  }
};
