import sha1 from "sha1";

import {
  submitSingleReview,
  createOrIncrementRating
} from "../repository/mongo-client";
import { errorHandler, getEntityName } from "../helpers";
import { HTTP_RESOURCE_CREATED, REVIEW_STATUS_CREATED } from "../constants";

export default async (req, res) => {
  try {
    const { tenant } = req.headers;
    const { author, entityId, title, description, rating } = req.body;
    const data = {
      author,
      entityId,
      title,
      reviewText: description,
      rating: Number(rating),
      tenant,
      reviewedDateTime: new Date().getTime(),
      sentimentAnalysisScore: 0,
      profanityAnalysisScore: 0,
      helpfulnessCount: 0,
      reportAbuseCount: 0,
      isPurchaseVerified: true
    };
    // data.sentimentAnalysis = SENTIMENT_ANALYSIS_NEUTRAL;
    data.reviewStatus = REVIEW_STATUS_CREATED;
    const insertedData = await insertReview(
      getEntityName(author, entityId),
      data
    );
    res.status(HTTP_RESOURCE_CREATED);
    return res.json(insertedData);
  } catch ({ message, code }) {
    console.error("Error while submitting review:", message);
    errorHandler(res, message);
  }
};

const insertReview = (_id, data) => {
  return new Promise(async (resolve, reject) => {
    const query = { _id: sha1(_id) };
    data.reviewId = _id;
    const newValues = { $set: data };
    try {
      //review is {created: bool, updated: bool}  to later decide whether to trigger count increase sideeffects
      const review = await submitSingleReview(query, newValues);
      if (review.created) {
        await createOrIncrementRating(data);
      }
      resolve(data);
    } catch (err) {
      reject(err);
    }
  });
};
