import { getReviewListWithCountBy } from "../repository/mongo-client";
import { errorHandler } from "../helpers";
import {
  REVIEW_STATUS_PENDING,
  REVIEW_STATUS_APPROVED,
  REVIEW_STATUS_REJECTED,
  REVIEW_STATUS_CREATED
} from "../constants";
export default async (req, res) => {
  const reviewStatusMap = {
    approved: REVIEW_STATUS_APPROVED,
    pending: REVIEW_STATUS_PENDING,
    created: REVIEW_STATUS_CREATED,
    rejected: REVIEW_STATUS_REJECTED
  };
  const { reviewStatus } = req.params;
  const { sortBy, limit, offset } = req.query;
  const query = { reviewStatus: reviewStatusMap[reviewStatus] };

  try {
    const { totalCount, reviewData } = await getReviewListWithCountBy(query, {
      sortBy,
      limit,
      offset
    });

    const sanitizedReviews = reviewData.map((raw) => {
      const { _id, tenant, ...rest } = raw;
      return rest;
    });
    return res.json({ totalCount, reviewData: sanitizedReviews });
  } catch ({ message, code }) {
    console.error("Error while getting the review list:", message);
    errorHandler(res, message);
  }
};
