import { patchReviewStatus } from "../repository/mongo-client";
import { errorHandler } from "../helpers";
import { DB_COLUMN } from "../constants";

export default async (req, res) => {
  const { reviewIds, reviewStatus } = req.body;
  const query = { [DB_COLUMN.REVIEW_ID]: { $in: reviewIds } };
  try {
    const updateResponse = await patchReviewStatus(query, reviewStatus);

    res.status(200);
    return res.json(updateResponse);
  } catch ({ message, code }) {
    console.error("Error while getting the review list:", message);
    errorHandler(res, message);
  }
};
