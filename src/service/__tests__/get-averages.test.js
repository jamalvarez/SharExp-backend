import getAverages from "../get-averages";

jest.mock("../../repository/mongo-client", () => ({getReviewListBy:({entityId}) => {
if(entityId=== "empty"){
  return []
} else {
  return [
    {
      _id: "6341983db5bc15d42cf7a7bae84fa4bad361b15a",
      author: "author1",
      description: "23123",
      entityId: "someentity",
      rating: 5,
      reviewId: "cl-product-author1-someentity",
      tenant: "cl",
      title: "string"
    },
    {
      _id: "81499ec0830c9a310f30d151d6ae73b73e168248",
      author: "author3",
      description: "23123",
      entityId: "someentity",
      rating: 1,
      reviewId: "cl-product-author3-someentity",
      tenant: "cl",
      title: "string"
    },
    {
      _id: "ede632a699d3e8d91df9c8e558b4c1b1d4dd749b",
      author: "author12",
      description: "23123",
      entityId: "someentity",
      rating: 4,
      reviewId: "cl-product-author12-someentity",
      tenant: "cl",
      title: "string"
    },
    {
      _id: "d582690ae6d858b267b738fa972c8dd5873d97d9",
      author: "author33",
      description: "23123",
      entityId: "someentity",
      rating: 1,
      reviewId: "cl-product-author33-someentity",
      tenant: "cl",
      title: "string"
    },
    {
      _id: "62cdf446957377355865b8af24345908a83d6b4c",
      author: "author33",
      description: "23123",
      entityId: "someentity",
      rating: 1,
      reviewId: "product-author33-someentity",
      reviewStatus: 1,
      sentimentAnalysis: 0,
      tenant: null,
      title: "string"
    }
  ]
}

}}));

jest.mock("mongodb");

const res = {
  status: jest.fn(),
  json: (averagesObject) => averagesObject
};

describe("get-averages service", () => {
  test("It returns the correct average object when the entityId has corresponding reviews with ratings", async () => {
    const req = {
      query: {
        entityId: "123"
      }
    };
    const averagesObject = await getAverages(req, res);
    expect(averagesObject).toMatchObject({
      totalNumberOfReviews: 5,
      averageRating: 2.4
    });
  });

  test("It returns the correct 0 values averages object entityId has no corresponding reviews with ratings", async () => {
    const req = {
      query: {
        entityId: "empty"
      }
    };
    const averagesObject = await getAverages(req, res);
    expect(averagesObject).toMatchObject({
      totalNumberOfReviews: 0,
      averageRating: 0
    });
  });
});
