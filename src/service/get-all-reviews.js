import {
  getReviewListBy,
  getReviewListWithCountBy
} from "../repository/mongo-client";
import { errorHandler, getAverages } from "../helpers";
import { HTTP_SUCCESS_CODE, REVIEW_STATUS_APPROVED } from "../constants";
import { processAverageReview } from "../service/get-averages";

const getAllReviews = (entityId, sortBy, offset, limit) => {
  return new Promise(async (resolve, reject) => {
    try {
      const { reviewData, totalCount } = await getReviewListWithCountBy(
        {
          entityId,
          reviewStatus: REVIEW_STATUS_APPROVED,
          reviewText: { $exists: true, $ne: "" }
        },
        { sortBy, offset, limit }
      );

      const sanitizedReviewArray = reviewData.map((rawReview) => {
        const { _id, ...rest } = rawReview;
        return rest;
      });

      const average = await processAverageReview(entityId, "v2");
      resolve({
        //	...average,
        totalNumberOfReviews: average.totalNumberOfReviews,
        averageRating: average.averageRating,
        totalNumberOfComments: totalCount,
        reviews: sanitizedReviewArray
      });
    } catch (err) {
      reject(err);
    }
  });
};

export default async (req, res) => {
  try {
    const { entityId, sortBy, offset, limit } = req.query;
    const reviewsResponse = await getAllReviews(
      entityId,
      sortBy,
      offset,
      limit
    );
    res.status(HTTP_SUCCESS_CODE);
    return res.json(reviewsResponse);
  } catch ({ message }) {
    console.error("Error while getting call reviews:", message);
    errorHandler(res, message);
  }
};
