import {
  submitVote,
  updateHelpfulnessCount
} from "../../repository/mongo-client";
import { errorHandler } from "../../helpers";

export default async (req, res) => {
  const { voterId, reviewId, voteType } = req.body;

  try {
    const voteTypeBooleans = {
      isAbusive: false,
      isHelpful: false,
      [voteType]: true
    };

    const query = {
      voterId,
      reviewId,
      _id: `${voterId}-${reviewId}`,
      ...voteTypeBooleans
    };
    await submitVote(query);
    const updatedCount = await updateHelpfulnessCount(
      { reviewId },
      voteType
    );

    res.status(200);
    return res.json({
      voteType,
      voterId,
      count: updatedCount
    });
  } catch ({ message, code }) {
    console.error("Error while posting review vote: ", message);
    res.status(403);
    res.json({
      error: "Error while posting review vote: Cannot vote again for the same review",
      code: 403,
      message: "ERROR"
    });
  }
};
