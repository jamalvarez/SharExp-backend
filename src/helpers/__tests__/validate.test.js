import validate from "../validate";
jest.mock("validatorjs", () => {
  return jest.fn().mockImplementation(({ body }) => {
    return {
      passes: (cb) => {
        if (body.tenant !== "bad") {
          cb(null, true);
        }
      },
      fails: (cb) => {
        if (body.tenant === "bad") {
          cb(true, false);
        }
      },
      errors: true
    };
  });
});
describe("validate helper function", () => {
  const validationRules = {
    author: "required|string",
    entityId: "required|string",
    rating: "min:0|max:5|required"
  };

  let res, next;

  beforeEach(() => {
    res = {
      status: jest.fn(() => ({ send: () => {} }))
    };
    next = jest.fn();
  });

  const correctBodies = [
    {
      body: {
        author: "author33",
        entityId: "anotherentity",
        title: "string",
        description: "23123",
        rating: 1,
        tenant: "cl"
      }
    },
    {
      body: {
        author: "author2",
        entityId: "someentity",
        title: "string",
        description: "23123",
        rating: 1,
        tenant: "cl"
      }
    }
  ];

  describe.each(correctBodies)(
    "goes to the next request handler function when request body complies with rules",
    (req) => {
      test("correct bodies", async () => {
        const validatedData = await validate(req, res, next, validationRules);
        expect(validatedData.status).toBeTruthy();
        expect(validatedData.err).toBeFalsy()
      });
    }
  );

  const incorrectBodies = [
    {
      body: {
        author: "author33",
        title: "string",
        description: "23123",
        rating: 1,
        tenant: "bad"
      }
    },
    {
      body: {
        entityId: "someentity",
        title: "string",
        description: "23123",
        rating: 1,
        tenant: "bad"
      }
    },
    {
      body: {
        author: "author33",
        entityId: "entity",
        title: "string",
        description: "23123",
        tenant: "bad"
      }
    }
  ];
  describe.each(incorrectBodies)(
    "calls res.send to send the status & error response when request body doesn't comply with rules ",
    (req) => {
      test("incorrect body", async () => {
        const validatedData = await validate(req, res, next, validationRules);
        expect(validatedData.err).toBeTruthy();
        expect(validatedData.status).toBeFalsy()
      });
    }
  );
});
