import { getEntityName, getAverages, errorHandler } from "../index";

import { INTERNAL_SERVER_ERROR } from "../../constants";

const reviewArray = [
  {
    _id: "6341983db5bc15d42cf7a7bae84fa4bad361b15a",
    author: "author1",
    description: "23123",
    entityId: "someentity",
    rating: 5,
    reviewId: "cl-product-author1-someentity",
    tenant: "cl",
    title: "string"
  },
  {
    _id: "81499ec0830c9a310f30d151d6ae73b73e168248",
    author: "author3",
    description: "23123",
    entityId: "someentity",
    rating: 1,
    reviewId: "cl-product-author3-someentity",
    tenant: "cl",
    title: "string"
  },
  {
    _id: "ede632a699d3e8d91df9c8e558b4c1b1d4dd749b",
    author: "author12",
    description: "23123",
    entityId: "someentity",
    rating: 4,
    reviewId: "cl-product-author12-someentity",
    tenant: "cl",
    title: "string"
  },
  {
    _id: "d582690ae6d858b267b738fa972c8dd5873d97d9",
    author: "author33",
    description: "23123",
    entityId: "someentity",
    rating: 1,
    reviewId: "cl-product-author33-someentity",
    tenant: "cl",
    title: "string"
  },
  {
    _id: "62cdf446957377355865b8af24345908a83d6b4c",
    author: "author33",
    description: "23123",
    entityId: "someentity",
    rating: 1,
    reviewId: "product-author33-someentity",
    reviewStatus: 1,
    sentimentAnalysis: 0,
    tenant: null,
    title: "string"
  }
];
describe.each([[1, 1]])("getEntityName", (authorId, entityId) => {
  let oldEnv = process.env;
  afterEach(() => {
    process.env = oldEnv;
  });

  test("it returns the correct value for the authorId and entityId pairs when no env.ENTITY has been given", () => {
    expect(getEntityName(authorId, entityId)).toBe(`${authorId}-${entityId}`);
  });
  test("it returns the correct value for the authorId and entityId pairs when an env.ENTITY has been given", () => {
    process.env.ENTITY = "product";
    expect(getEntityName(authorId, entityId)).toBe(
      `${process.env.ENTITY}-${authorId}-${entityId}`
    );
  });
});

describe.each([
  [reviewArray, { totalNumberOfReviews: 5, averageRating: 2.4 }],
  [[], { totalNumberOfReviews: 0, averageRating: 0 }]
])("getAverages", (reviewsArray, expectedObject) => {
  test("it returns the correct number o reviews and the correct rating for the array of reviews that it gets as an argument", () => {
    expect(getAverages(reviewsArray)).toMatchObject(expectedObject);
  });
});


describe("errorhandler", () => {
  test("it calls the appropriate side effects methods for the response objet", () => {
    const res = {status: jest.fn(), json: jest.fn()}
    const message = "my test error message"
    errorHandler(res, message);

    expect(res.status).toHaveBeenCalledWith(INTERNAL_SERVER_ERROR)
    expect(res.json).toHaveBeenCalledWith({
      error: message,
      code: INTERNAL_SERVER_ERROR,
      message: "INTERNAL_SERVER_ERROR"
    })
  })
})