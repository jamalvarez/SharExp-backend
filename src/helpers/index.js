import { INTERNAL_SERVER_ERROR } from "../constants";

export const getEntityName = (authorId, entityId) =>
  `${
    process.env.ENTITY ? `${process.env.ENTITY}-` : ""
  }${authorId}-${entityId}`;

export const getAverages = (arrayOfReviews) => {
  const totalNumberOfReviews = arrayOfReviews.length;
  const rawAverageRating =
    arrayOfReviews.reduce((prev, { rating }) => prev + rating, 0) /
    totalNumberOfReviews;
  const averageRating = totalNumberOfReviews
    ? Number(rawAverageRating.toFixed(2))
    : 0;
  return {
    totalNumberOfReviews,
    averageRating
  };
};

export const errorHandler = (res, message) => {
  res.status(INTERNAL_SERVER_ERROR);
  res.json({
    error: message,
    code: INTERNAL_SERVER_ERROR,
    message: "INTERNAL_SERVER_ERROR"
  });
};