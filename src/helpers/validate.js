import Validator from "validatorjs";

const validator = (body, rules, customMessages, callback) => {
  const validation = new Validator(body, rules, customMessages);
  validation.passes(() => callback(null, true));
  validation.fails(() => callback(validation.errors, false));
};

export default (request, validationRule, customMessage = {}) => {
  return new Promise((resolve, reject) => {
    validator(request, validationRule, customMessage, (err, status) => {
      resolve({
        err,
        status
      });
    });
  });
};
