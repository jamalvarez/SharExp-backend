import sentimentAnalysis from "../helpers/sentiment-analysis";
import { updateReviewStatusWithSentimentValue } from "../repository/mongo-client";
import {
  SENTIMENT_ANALYSIS_POSITIVE,
  SENTIMENT_ANALYSIS_NEGATIVE,
  REVIEW_STATUS_PENDING,
  REVIEW_STATUS_APPROVED
} from "../constants";

export default (review) => {
  return new Promise(async (resolve, reject) => {
    try {
      console.log(`Processing review: ${review.entityId}`);
      const sentimentResult = sentimentAnalysis(review.description);
      let reviewStatus = REVIEW_STATUS_PENDING;
      if (sentimentResult >= 0) {
        reviewStatus = REVIEW_STATUS_APPROVED;
      }
      if (sentimentResult < 0) {
        reviewStatus = REVIEW_STATUS_PENDING;
      }
      await updateReviewStatusWithSentimentValue(
        review._id,
        sentimentResult,
        reviewStatus
      );
      resolve("OK");
    } catch (error) {
      console.error("Error while processing review: ", review, " error", error);
      reject(error);
    }
  });
};
