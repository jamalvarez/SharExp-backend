import { MongoClient } from "mongodb";
import {
  RATINGS_COLUMN_MAPPING,
  REVIEW_STATUS_CREATED,
  SORT_OPTIONS,
  DB_COLUMN
} from "../constants";

require("dotenv").config();

let dbConnection, mongoClient;

MongoClient.connect(process.env.MONGO_DB_URL, function (err, client) {
  if (err) {
    console.error("error while connecting to mongodb::: ", err);
  } else {
    mongoClient = client;
    dbConnection = client.db(process.env.MONGO_DATABASE);
    console.log("Connected to mongodb server");
  }
});

const getReviewListBy = async (query, options = {}) => {
  const { sortBy, offset, limit } = options;
  const listToBeReturned = await dbConnection
    .collection(`${process.env.ENTITY}_reviews`)
    .find(query)
    .sort(sortBy && SORT_OPTIONS[sortBy])
    .skip(parseInt(offset || 0, 10))
    .limit(parseInt(limit || 20, 10))
    .toArray();
  return listToBeReturned;
};

const getReviewListWithCountBy = async (query, options = {}) => {
  const { sortBy, offset, limit } = options;
  const listCursor = await dbConnection
    .collection(`${process.env.ENTITY}_reviews`)
    .find(query)
    .sort(sortBy && SORT_OPTIONS[sortBy])
    .skip(parseInt(offset || 0, 10))
    .limit(parseInt(limit || 20, 10));

  const totalCount = await listCursor.count();
  const reviewData = await listCursor.toArray();

  return { totalCount, reviewData };
};

const getSingleReviewBy = async (query) => {
  const reviewToReturn = await dbConnection
    .collection(`${process.env.ENTITY}_reviews`)
    .findOne(query);
  return reviewToReturn || {};
};

const submitSingleReview = async (query, newValues) => {
  const { upsertedId, matchedCount } = await dbConnection
    .collection(`${process.env.ENTITY}_reviews`)
    .updateOne(query, newValues, { upsert: true });
  return { created: !!upsertedId, update: !!matchedCount };
};

const createOrIncrementRating = async (data) => {
  const query = {
    entityId: data.entityId
  };
  const updateValues = {
    $inc: {
      [RATINGS_COLUMN_MAPPING[data.rating]]: 1
    }
  };
  const { upsertedId, matchedCount } = await dbConnection
    .collection(`${process.env.ENTITY}_ratings`)
    .findOneAndUpdate(query, updateValues, { upsert: true });
  return { created: !!upsertedId, update: !!matchedCount };
};

const getRatingByEntity = async (entityId) => {
  const query = {
    entityId
  };
  const result = dbConnection
    .collection(`${process.env.ENTITY}_ratings`)
    .findOne(query);
  return result || {};
};

const getReviewsByCreatedStatus = async () => {
  const listCursor = await dbConnection
    .collection(`${process.env.ENTITY}_reviews`)
    .find({
      reviewStatus: REVIEW_STATUS_CREATED
    })
    .limit(parseInt(process.env.CRON_JOB_REVIEWS_PROCESS_LIMIT || 10, 10));
  const totalCount = await listCursor.count();
  const reviews = await listCursor.toArray();
  return { totalCount, reviews };
};

const updateReviewStatusWithSentimentValue = async (
  id,
  sentimentAnalysisScore,
  reviewStatus
) =>
  new Promise((resolve, reject) => {
    dbConnection.collection(`${process.env.ENTITY}_reviews`).findOneAndUpdate(
      {
        _id: id
      },
      { $set: { reviewStatus, sentimentAnalysisScore } },
      {},
      function (err, doc) {
        if (err) {
          reject(err);
        } else {
          resolve("Updated");
        }
      }
    );
  });

const patchReviewStatus = async (query, newValue) => {
  const updateCursor = await dbConnection
    .collection(`${process.env.ENTITY}_reviews`)
    .updateMany(query, {
      $set: { [DB_COLUMN.STATUS]: newValue }
    });

  return { code: 200, message: "successufully updated status" };
};

const submitVote = async (query) => {
  const { insertedId } = await dbConnection
    .collection(`${process.env.ENTITY}_votes`)
    .insertOne(query);
  return !!insertedId;
};

const updateHelpfulnessCount = async (query, voteType) => {
  const counterToUpdate = {
    isHelpful: "helpfulnessCount",
    isAbusive: "reportAbuseCount"
  };

  const updateDocument = {
    $inc: {
      [counterToUpdate[voteType]]: 1
    }
  };

  const { value } = await dbConnection
    .collection(`${process.env.ENTITY}_reviews`)
    .findOneAndUpdate(query, updateDocument, { returnOriginal: false });
    const modifiedCount = value[counterToUpdate[voteType]];
  return modifiedCount;
};
export {
  mongoClient,
  dbConnection,
  submitVote,
  updateHelpfulnessCount,
  getReviewListBy,
  getReviewListWithCountBy,
  getSingleReviewBy,
  submitSingleReview,
  createOrIncrementRating,
  getRatingByEntity,
  getReviewsByCreatedStatus,
  patchReviewStatus,
  updateReviewStatusWithSentimentValue
};
