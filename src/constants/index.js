export const REVIEW_STATUS_PENDING = 0;
export const REVIEW_STATUS_APPROVED = 1;
export const REVIEW_STATUS_REJECTED = 2;
export const REVIEW_STATUS_CREATED = 3;

export const SENTIMENT_ANALYSIS_NEUTRAL = 0;
export const SENTIMENT_ANALYSIS_POSITIVE = 1;
export const SENTIMENT_ANALYSIS_NEGATIVE = 2;

export const INTERNAL_SERVER_ERROR = 500;
export const HTTP_SUCCESS_CODE = 200;
export const HTTP_NOT_FOUND = 404;
export const HTTP_RESOURCE_CREATED = 201;
export const HTTP_BAD_REQUEST = 400;

export const SORT_OPTIONS_MOST_RECENT = "most-recent";
export const SORT_OPTIONS_HIGHEST_RATED = "highest-rated";
export const SORT_OPTIONS_LOWEST_RATED = "lowest-rated";
export const SORT_OPTIONS_MOST_HELPFUL = "most-helpful";
export const SORT_OPTIONS_MOST_POSITIVE = "most-positive";
export const SORT_OPTIONS_MOST_NEGATIVE = "most-negative";

export const SORT_OPTIONS = {
  [SORT_OPTIONS_MOST_RECENT]: { reviewedDateTime: -1 },
  [SORT_OPTIONS_HIGHEST_RATED]: { rating: -1 },
  [SORT_OPTIONS_LOWEST_RATED]: { rating: 1 },
  [SORT_OPTIONS_MOST_HELPFUL]: { helpfulnessCount: -1 },
  [SORT_OPTIONS_MOST_POSITIVE]: { sentimentAnalysisScore: -1 },
  [SORT_OPTIONS_MOST_NEGATIVE]: { sentimentAnalysisScore: 1 }
};

export const DB_COLUMN = {
  ID: "_id",
  AUTHOR: "author",
  DESCRIPTION: "description",
  ENTITY_ID: "entityId",
  RATING: "rating",
  REVIEW_ID: "reviewId",
  TITLE: "title",
  STATUS: "reviewStatus"
};

export const API_CONTRACT = {
  ID: "_id",
  AUTHOR: "author",
  DESCRIPTION: "description",
  ENTITY_ID: "entityId",
  RATING: "rating",
  REVIEW_ID: "reviewId",
  TITLE: "title",
  STATUS: "reviewStatus"
};

export const VALIDATION_SUCCESS = "validation-success";
export const VALIDATION_FAILURE = "validation-failure";

export const RATINGS_COLUMN_MAPPING = {
  5: "fiveStarReviewCount",
  4: "fourStarReviewCount",
  3: "threeStarReviewCount",
  2: "twoStarReviewCount",
  1: "oneStarReviewCount"
};
